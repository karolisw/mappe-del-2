import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import ntnu.mappe2.karolisw.Patient;
import ntnu.mappe2.karolisw.PatientRegisterInfo;
import ntnu.mappe2.karolisw.PopUpWindowController;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class PopUpWindowControllerTest {
    PopUpWindowController popUpWindowController = new PopUpWindowController();
    ObservableList<Patient> patients = FXCollections.observableArrayList();

    Patient patient = new Patient("Karoline","Wahl","",
            "Eyvind Vigås", "12345678910");


    /**
     * New file is created, and exported to.
     * Asserts true that the file exists
     */
    @Test
    void newFileIsCreated(){
        File newFile = popUpWindowController.createNewFile("src/main/resources/csv/","patientTestFile2.csv");
        popUpWindowController.exportAllPatientsToCSV(newFile);
        assertTrue(newFile.exists());
    }

    /**
     * There are 3 rows (3 patients) in patientsTest.csv
     * Therefore, there should be 3 patients imported
     */
    @Test
    void importsCorrectAmount() throws IOException {
        patients = popUpWindowController.importFromCsv("src/main/resources/csv/patientsTest.csv");
        assertEquals(patients.size(),3);
    }


    /**
     * Tests that patient is exported to an empty csv file.
     * Verified by importing and checking that size is the same as expected
     * @throws IOException if file exists
     */
    @Test
    void patientExportsToNewCsvFile() throws IOException {
        File newFile2 = popUpWindowController.createNewFile("src/main/resources/csv","patientTestFile3");

        PatientRegisterInfo.getAllPatients().add(patient);
        popUpWindowController.exportAllPatientsToCSV(newFile2);
        patients = popUpWindowController.importFromCsv("src/main/resources/csv/patientTestFile3.csv");
        assertEquals(patients.size(),2);
    }
}
