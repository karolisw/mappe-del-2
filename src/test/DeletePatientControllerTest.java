import ntnu.mappe2.karolisw.Patient;
import ntnu.mappe2.karolisw.PatientRegisterInfo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DeletePatientControllerTest {

    /**
     * Tests a much used method, removeSelectedPatients()
     */
    @Test
    void isPatientRemovedFromListWhenDeleted(){
        Patient patient = new Patient("Karoline","Wahl","",
                "Eyvind Vigås", "12345678910");
        PatientRegisterInfo.getSelectedPatients().add(patient);
        PatientRegisterInfo.removeSelectedPatients();

        Assertions.assertEquals(null,PatientRegisterInfo.getSelectedPatients());
    }
}
