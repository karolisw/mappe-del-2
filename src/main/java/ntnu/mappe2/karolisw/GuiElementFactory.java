package ntnu.mappe2.karolisw;

import javafx.scene.Node;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TableView;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

public class GuiElementFactory {

    /**
     * Node factory produses nodes for GUI
     *
     * @param elementToBeCreated could be:
     *                           tableview, toolbar, borderpane, vbox, menubar
     * @return element requested in parameter unless it is null
     * @throws IllegalArgumentException if argument is not in parameter or null
     */
    public static Node createElement(String elementToBeCreated) throws IllegalArgumentException{
            String element = elementToBeCreated.toLowerCase();
            if(element.isEmpty()){ //TODO test if this is reached when elementotbecreated is an empty string
                return null;
            }
            else if(element.equals("tableview")){
                return new TableView<>();
            }
            else if(element.equals("toolbar")){
                return new ToolBar();
            }
            else if(element.equals("borderpane")){
                return new BorderPane();
            }
            else if(element.equals("vbox")){
                return new VBox();
            }
            else if(element.equals("menubar")){
                return new MenuBar();
            }
            else throw new IllegalArgumentException();
        }

}
