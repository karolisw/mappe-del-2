package ntnu.mappe2.karolisw;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

/**
 * Pops up when:
 * trying to change scene without selecting
 * trying to import from a file that is not a .csv file
 */
public class PopUpWindowController implements Initializable {

    SceneChanger sceneChanger = new SceneChanger();


    // Labels
    @FXML private Label nameLabel;
    @FXML private Label pathLabel;
    @FXML private Label successLabel;
    @FXML private Label totalPatientsLabel;
    @FXML private Label recentPatientsLabel;
    @FXML private Label wrongInputLabel;

    // Buttons
    @FXML private Button importButton;
    @FXML private Button exportButton;
    @FXML private Button cancelButton;
    @FXML private Button OkButton;

    @FXML private TextField pathField;
    @FXML private TextField nameField;

    /**
     * When changing scenes for showing warning about existing file when exporting,
     * this is where the values are saved
     */
    private static String pathSave = null;
    private static String nameSave = null;

    public static void setNameSave(String name) {
        nameSave = name;
    }

    public static void setPathSave(String path) {
        pathSave = path;
    }

    public static void removeSavedPathAndName() {
        pathSave = null;
        nameSave = null;
    }


    /**
     * Method used to not rewrite too much code.
     * @param event button clicked (could be any button)
     */
    @FXML
    public void exitToRegister(ActionEvent event) {
        try {
            sceneChanger.changeScene(event, "patientRegister.fxml", "Patient register");
            removeSavedPathAndName();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends user to the export pop up window
     * @param event
     */
    @FXML
    public void exitToCsvExport(ActionEvent event) {
        try {
            sceneChanger.changeScene(event, "csvExportPopUp.fxml", "Export data");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Method for reading patients from a csv file
     * Will only work with .csv
     *
     * @param pathFromUser name of the file we're reading from
     *                     data array contains list of fields in each row of file in pathToCsv
     *                     delimiter = ';'
     * @throws IOException if file not found
     */
    public ObservableList<Patient> importFromCsv(String pathFromUser) throws IOException {
        String line;
        ObservableList<Patient> patientsFromFile = FXCollections.observableArrayList();
        BufferedReader csvReader = new BufferedReader(new FileReader(pathFromUser));

        try {
            while ((line = csvReader.readLine()) != null) {
                String[] patientAsList = line.split(";");
                Patient patient = convertListToPatient(patientAsList);
                patientsFromFile.add(patient);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return patientsFromFile;
    }


    /**
     * When the user wants to export their data, they need a file to save it in, and this is that file
     * The method also checks for errors in user input, such as lack of "/" or if they forgot to put ".csv" in filename
     * @param directoryPath is the path to the desired directory that will hold this new file
     * @param fileName the name of the file that the user wants to save their exported data in .csv format
     * @return the newly created File
     */
    public File createNewFile(String directoryPath, String fileName) {
        if (!directoryPath.endsWith("/")) {
            String fileCsvPath = directoryPath + "/";
            directoryPath = fileCsvPath;
        }
        if (!fileName.endsWith(".csv")) {
            String fileCsvType = fileName + ".csv";
            fileName = fileCsvType;
        }
        return new File(directoryPath + fileName);
    }


    /**
     * Method that returns the FileWriter when exporting
     * Has it's own method because of cohesion (there are three methods that need this)
     * @param file the created file in createNewFile-method (or a file to be overwritten)
     * @return the FileWriter
     *
     * @throws IOException if the File does not exist (path is wrong etc.)
     */
    public FileWriter Filewriter(File file) throws IOException {
        FileWriter csvWriter = new FileWriter(file, false);
        csvWriter.append("firstName");
        csvWriter.append(";");
        csvWriter.append("lastName");
        csvWriter.append(";");
        csvWriter.append("diagnosis");
        csvWriter.append(";");
        csvWriter.append("generalPractitioner");
        csvWriter.append(";");
        csvWriter.append("socialSecurityNumber");
        csvWriter.append("\n");
        return csvWriter;
    }

    /**
     * Method for writing patients to a csv file
     * Will only work with .csv
     * Used FileWriter method to create file-writer
     *
     * @param file file we're saving to
     */
    @FXML
    public void exportAllPatientsToCSV(File file) {
        try {
            List<List<String>> rows = PatientRegisterInfo.getAllPatientsToList();
            FileWriter csvWriter = Filewriter(file);
            for (List<String> rowData : rows) {
                csvWriter.append(String.join(";", rowData));
                csvWriter.append("\n");
            }
            csvWriter.flush();
            csvWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * When user has selected to only export the newly added patients, this method is called upon
     * @param file is the file the user wants to write to (could be an overwrite)
     */
    @FXML
    public void exportNewPatientsToCsv(File file) {
        try {
            List<List<String>> rows = PatientRegisterInfo.getNewPatientsToList();
            FileWriter csvWriter = Filewriter(file);
            for (List<String> rowData : rows) {
                csvWriter.append(String.join(";", rowData));
                csvWriter.append("\n");
            }
            csvWriter.flush();
            csvWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * When importing, the data from the .csv file is converted to Patient thorugh this method
     * @param patient is the string from a row from the .csv-file
     * @return a Patient object
     */
    public Patient convertListToPatient(String[] patient) {
        ArrayList<String> patientAsArray = new ArrayList<>(Arrays.asList(patient));
        return new Patient(patientAsArray.get(0), patientAsArray.get(1), "", patientAsArray.get(2), patientAsArray.get(3));
    }


    /**
     * Called upon (scene-change) when the user enters a desired export-location that is already in use
     * Either exports all, or only new patients, depending on what the user chose
     */
    public void overwriteSavedFile(ActionEvent event) {
        File fileToOverWrite = createNewFile(pathSave, nameSave);

        if (PatientRegisterInfo.getSelectedPatients().equals(PatientRegisterInfo.getAllPatients())) {
            exportAllPatientsToCSV(fileToOverWrite);
        } else {
            exportNewPatientsToCsv(fileToOverWrite);
        }

        exitToRegister(event);
    }

    /**
     * Checks if the path and filename from export-view is an existent file or not
     * Also checks if it is a directory (isRegularFile)
     * @param pathName is the path to directory the user put in
     * @param fileName the name of the file that the user wanted to save to
     * @return a boolean for whether the file exists or not
     */
    public boolean fileExistsAtPath(String pathName, String fileName) {
        File file = new File(pathName + "/" + fileName);
        return Files.exists(file.toPath()) && Files.isRegularFile(file.toPath());
    }

    /**
     * Same as method above, but used in importButtonClicked-method
     * @param pathName is entered by the user
     * @return boolean for whether or not hte file exists
     */
    public boolean fileExists(String pathName) {
        File file = new File(pathName);
        return file.exists() && !file.isDirectory();
    }


    /**
     * Checks if file exists at desired path. If it does, overwrite view is entered.
     * Path and file name is saved for the duration the user spends deciding (static variables)
     * Else, the file is exported here
     *
     * @param event export button clicked
     */
    @FXML
    public void exportPatientsButtonClicked(ActionEvent event) {
        try {
            if (PatientRegisterInfo.getSelectedPatients().equals(PatientRegisterInfo.getAllPatients())) {
                checkForAddingAll(event);
            } else { //if only the new ones were selected previously
                if (fileExistsAtPath(pathField.getText(), nameField.getText())) {
                    PopUpWindowController.setNameSave(nameField.getText());
                    PopUpWindowController.setPathSave(pathField.getText());
                    sceneChanger.changeScene(event, "overwriteExistingCsvFile.fxml", "Existing file warning");
                } else {
                    File newFile = createNewFile(pathField.getText(), nameField.getText());
                    exportNewPatientsToCsv(newFile);
                    exitToRegister(event);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * If this button is selected, the selected patients are set to only be the new ones
     * The user in sent over to the export-view
     */
    @FXML
    public void exportRecentPatientsButtonClicked(ActionEvent event) {
        try {
            PatientRegisterInfo.setSelectedPatients(PatientRegisterInfo.getNewPatients());
            sceneChanger.changeScene(event, "csvExportPopUp.fxml", "Export to '.csv'-file");        //change scene til export fila
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * If this button is selected, the selected patients are set to only be all patients
     * The user is sent over to the export-view
     */
    @FXML
    public void exportAllPatientsButtonClicked(ActionEvent event) {
        try {
            PatientRegisterInfo.setSelectedPatients(PatientRegisterInfo.getAllPatients());
            sceneChanger.changeScene(event, "csvExportPopUp.fxml", "Export to '.csv'-file");        //change scene til export fila
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * If file exists, we must overwrite, else we add
     *
     * @throws IOException if the fxml loader cannot find the desired fxml-sheet
     */
    private void checkForAddingAll(ActionEvent event) throws IOException {
        if (fileExistsAtPath(pathField.getText(), nameField.getText())) {
            PopUpWindowController.setNameSave(nameField.getText());
            PopUpWindowController.setPathSave(pathField.getText());
            sceneChanger.changeScene(event, "overwriteExistingCsvFile.fxml", "Existing file warning");

        } else {
            File newFile = createNewFile(pathField.getText(), nameField.getText());
            exportAllPatientsToCSV(newFile);
            exitToRegister(event);

        }
    }

    /**
     * Imported csv files are only added to the list of all patients the first time
     * After that every import is considered an import of new patients
     */
    @FXML
    public void importButtonClicked(ActionEvent event) {
        try {
            if (fileExists(pathField.getText())) {
                ObservableList<Patient> patientsFromCsv = importFromCsv(pathField.getText());
                if (PatientRegisterInfo.getAllPatients().size() == 0) {
                    PatientRegisterInfo.getAllPatients().addAll(patientsFromCsv);
                } else {
                    PatientRegisterInfo.getAllPatients().addAll(patientsFromCsv);
                    PatientRegisterInfo.getNewPatients().addAll(patientsFromCsv);
                }
                exitToRegister(event);

            } else {
                wrongInputLabel.setText("There is no .csv file here");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * If the user had added a new patient before wanting to export, this will be initialized
     * This is only for adding understanding through graphic representation
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            recentPatientsLabel.setText(String.valueOf(PatientRegisterInfo.getNewPatients().size()));
            totalPatientsLabel.setText(String.valueOf(PatientRegisterInfo.getAllPatients().size()));
        } catch (Exception ignore) {
        }

    }
}

