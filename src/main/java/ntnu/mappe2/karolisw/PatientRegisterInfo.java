package ntnu.mappe2.karolisw;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Singleton for saving patient objects when changing between scenes
 */

public class PatientRegisterInfo {

    private static SceneChanger sceneChanger = new SceneChanger();
    private static String addOrEditPatient;


    /**
     * Holds the selected patients from checkboxes in registerPatientController
     */
    private static ObservableList<Patient> selectedPatients = FXCollections.observableArrayList();


    /**
     * ArrayLists making it easy to keep track of all patients vs new patients
     * adds functionality for writing all patients, or just the ones added during program runtime to file
     */
    private static ObservableList<Patient> allPatients = FXCollections.observableArrayList();
    private static ObservableList<Patient> newPatients = FXCollections.observableArrayList();



    /**
     * Constructor hinders creation of objects of this class
     */
    private PatientRegisterInfo(){
        throw new IllegalStateException("This class cannot be instantiated");
    }


    /**
     * Get/accessor methods
     */
    public static ObservableList<Patient> getSelectedPatients() {
        return selectedPatients;
    }

    public static ObservableList<Patient> getAllPatients() {
        return allPatients;
    }

    public static ObservableList<Patient> getNewPatients() {
        return newPatients;
    }

    public static List<List<String>> getAllPatientsToList() throws NullPointerException{
        return getLists(allPatients);
    }

    public static List<List<String>> getNewPatientsToList() throws NullPointerException{
        return getLists(newPatients);
    }

    public static String getAddOrEditPatient() {
        return addOrEditPatient;
    }


    /**
     * Set/mutator methods
     */
    public static void setSelectedPatients(ObservableList<Patient> selectedPatients) {
        PatientRegisterInfo.selectedPatients = selectedPatients;
    }

    public static void setAddOrEditPatient(String addOrEdit) {
        addOrEditPatient = addOrEdit;
    }


    /**
     * Methods for setting status and selection of patients equal to null
     */
    public static void removeSelectedPatients(){
        selectedPatients = null;
    }

    public static void removeAddOrEditStatus(){
        addOrEditPatient = null;
    }


    /**
     * Removes patient from allPatients and newPatients list
     */
    public static void deleteSelectedPatients(){
        try{
            if(selectedPatients != null){
                allPatients.removeAll(selectedPatients);
                newPatients.removeAll(selectedPatients);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    /**
     * Turns an ObservableList of Patient objects into list
     * Used for exporting patients to csv file
     * getAllPatientsToList and getNewPatientsToList methods use this method
     * @param ObservableListOfPatients is the list we want to transform
     * @return a a List<List<String>>
     */
    private static List<List<String>> getLists(ObservableList<Patient> ObservableListOfPatients) {
        List<List<String>> newListOfPatients = new ArrayList<>();
        for(Patient patient : ObservableListOfPatients){
            List<String> patientAsList = new ArrayList<>();
            patientAsList.add(patient.getFirstName());
            patientAsList.add(patient.getLastName());
            patientAsList.add(patient.getDiagnosis());
            patientAsList.add(patient.getGeneralPractitioner());
            patientAsList.add(patient.getSocialSecurityNumber());
            newListOfPatients.add(patientAsList);
        }
        return newListOfPatients;
    }
}
