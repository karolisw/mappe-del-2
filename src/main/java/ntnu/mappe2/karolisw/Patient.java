package ntnu.mappe2.karolisw;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

public class Patient {
    private SimpleStringProperty firstName;
    private SimpleStringProperty lastName;
    private SimpleStringProperty diagnosis;
    private SimpleStringProperty socialSecurityNumber;
    private SimpleStringProperty generalPractitioner;
    private SimpleBooleanProperty selected;

    /**
     * Getters/accessor-methods
     */
    public boolean isSelected() {
        return selected.get();
    }

    public String getFirstName() {
        return firstName.get();
    }

    public String getLastName() {
        return lastName.get();
    }

    public String getDiagnosis() {
        return diagnosis.get();
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber.get();
    }

    public String getGeneralPractitioner() {
        return generalPractitioner.get();
    }


    /**
     * Setters/mutator-methods
     */

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis.set(diagnosis);
    }

    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber.set(socialSecurityNumber);
    }

    public void setSelected(boolean selected) {
        this.selected.set(selected);
    }

    public void setGeneralPractitioner(String generalPractitioner) {
        this.generalPractitioner.set(generalPractitioner);
    }

    /**
     * Constructor for Patient-object
     * Parameter-testing happens in PatientDetailsController
     *
     * @param firstName (required)
     * @param lastName  (required)
     * @param diagnosis diagnosis is set by general practitioner
     * @param generalPractitioner sets diagnosis on/for patient
     * @param socialSecurityNumber identifies patient (required)
     *
     * booleanProperty selected is part of object due to register checkboxes
     *                             selected is set to true when user is selected and vice-versa
     */
    public Patient(String firstName, String lastName, String diagnosis,
                   String generalPractitioner, String socialSecurityNumber) {
        this.firstName = new SimpleStringProperty(firstName);
        this.lastName = new SimpleStringProperty(lastName);
        this.diagnosis = new SimpleStringProperty(diagnosis);
        this.generalPractitioner = new SimpleStringProperty(generalPractitioner);
        this.socialSecurityNumber = new SimpleStringProperty(socialSecurityNumber);
        selected = new SimpleBooleanProperty(false);
    }


    /**
     * equals-method compares objects of Patient with all fields (parameters) included
     *
     * @param o Patient-object to be compared with "this" Patient-object
     * @return true/false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Patient)) return false;

        Patient patient = (Patient) o;

        if (firstName != null ? !firstName.equals(patient.firstName) : patient.firstName != null) return false;
        if (lastName != null ? !lastName.equals(patient.lastName) : patient.lastName != null) return false;
        if (diagnosis != null ? !diagnosis.equals(patient.diagnosis) : patient.diagnosis != null) return false;
        if (socialSecurityNumber != null ? !socialSecurityNumber.equals(patient.socialSecurityNumber) : patient.socialSecurityNumber != null)
            return false;
        return generalPractitioner != null ? generalPractitioner.equals(patient.generalPractitioner) : patient.generalPractitioner == null;
    }

    @Override
    public int hashCode() {
        int result = firstName != null ? firstName.hashCode() : 0;
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (diagnosis != null ? diagnosis.hashCode() : 0);
        result = 31 * result + (socialSecurityNumber != null ? socialSecurityNumber.hashCode() : 0);
        result = 31 * result + (generalPractitioner != null ? generalPractitioner.hashCode() : 0);
        return result;
    }

    /**
     * Method prints out patient in a clean way
     *
     * @return patient as a String
     */
    @Override
    public String toString() {
        return "Patient :" +
                "First name: " + firstName +
                ", lastName: " + lastName +
                " \ndiagnosis: " + diagnosis +
                ", socialSecurityNumber: " + socialSecurityNumber +
                " \ngeneralPractitioner: " + generalPractitioner +
                ", selected: " + selected +"\n\n";
    }
}
