package ntnu.mappe2.karolisw;

import java.io.*;
import java.net.URL;
import java.util.*;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class PatientRegisterController implements Initializable {

    // Menu
    @FXML MenuBar menuBar;
    @FXML Menu fileMenu;
    @FXML Menu editMenu;
    @FXML Menu helpMenu;
    @FXML MenuItem importCSVItem;
    @FXML MenuItem exportCSVItem;
    @FXML MenuItem exitItem;
    @FXML MenuItem addPatientItem;
    @FXML MenuItem editPatientItem;
    @FXML MenuItem removePatientItem;
    @FXML SeparatorMenuItem fileMenuSeparator;

    // TableView-related
    @FXML TableView<Patient> tableView;
    @FXML TableColumn<Patient,String> firstNameColumn;
    @FXML TableColumn<Patient,String> lastNameColumn;
    @FXML TableColumn<Patient,String> socialSecurityNumberColumn;
    @FXML TableColumn<Patient, CheckBox> selectedColumn = new TableColumn<Patient, CheckBox>( "selected" );

    // Buttons
    @FXML Button addPatientButton;
    @FXML Button editPatientButton;
    @FXML Button removePatientButton;

    // Label
    @FXML Label errorLabel;


    /**
     * On scene initialized, the columns are set up, and the CheckBox-listener is called upon
     * Data loaded into TableView
     * TableView not editable, because edit-functionality exists
     * User is able to select multiple patients (because of multiple-deletion functionality)
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<Patient, String>("firstName"));
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<Patient, String>("lastName"));
        socialSecurityNumberColumn.setCellValueFactory(new PropertyValueFactory<Patient, String>("socialSecurityNumber"));

        checkboxListener();

        updateTableView();

        tableView.setEditable(false);
        tableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

    }

    /**
     * Listener that listens for change in a patients' selected-state
     * Listens through CheckBox column
     */
    @FXML
    public void checkboxListener(){
        selectedColumn.setCellValueFactory(arg0 -> {
            Patient patient = arg0.getValue();

            CheckBox checkBox = new CheckBox();

            checkBox.selectedProperty().setValue(patient.isSelected());


            checkBox.selectedProperty().addListener((ov, old_val, new_val) -> patient.setSelected(new_val));

            return new SimpleObjectProperty<>(checkBox);
        });
    }

    /**
     * Updates TableView by calling on allPatients from PatientRegisterInfo
     */
    public void updateTableView(){
        tableView.setItems(null);
        tableView.setItems(PatientRegisterInfo.getAllPatients());
    }

    /**
     * ObservableList of Patient-objects is filled with selected patients
     */
    @FXML
    public ObservableList<Patient> getSelectedCheckboxes(){
        ObservableList<Patient> selectedPatients = FXCollections.observableArrayList();
        try{
            for(Patient patient : tableView.getItems()){
                if(patient.isSelected()){
                    selectedPatients.add(patient);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return selectedPatients;
    }


    /**
     * Sets current patient for easier insertion into fields in next scene
     */
    @FXML private void addPatientButtonClicked(){
        try {
            PatientRegisterInfo.setAddOrEditPatient("add");
            changeSceneForMenuItem("patientDetails.fxml", "New patient");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * If there is one patient selected, this method will take the user over to the patient-detail view
     * If more than one or zero is selected, the method will set the error label above the TableView
     */
    @FXML private void editPatientButtonClicked(){
        try {
            if(getSelectedCheckboxes().size() > 0 && getSelectedCheckboxes().size() < 2){
                PatientRegisterInfo.setSelectedPatients(getSelectedCheckboxes());
                PatientRegisterInfo.setAddOrEditPatient("edit");
                changeSceneForMenuItem("patientDetails.fxml", "Edit patient");
            }
            else {
                errorLabel.setText("When editing patient information, only one (1) patient can be selected");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * If more than zero patients are selected, the method changes view to deletion-view, where the user
     * can choose to proceed with the deletion or not
     */
    @FXML
    private void removePatientButtonClicked(){
        try {
            if(getSelectedCheckboxes().size() > 0){
                PatientRegisterInfo.setSelectedPatients(getSelectedCheckboxes());
                changeSceneForMenuItem("deletePatient.fxml", "Delete patient");
            }
            else {
                errorLabel.setText("When deleting patient, one (1) ore more patients must be selected");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Shows about-view
     */
    @FXML
    public void helpClicked(){
        try {
            changeSceneForMenuItem("help.fxml", "Information Dialog - About");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * User exits the application
     */
    @FXML
    public void exitClicked(){
        System.exit(0);
    }


    /**
     * Due to not all elements in register inheriting from .Node, this method takes care of the Scene-changes for them
     * @param fxml the desired destination
     * @param title that fits for the desired destination
     *
     * @throws IOException if resource (fxml) does not exist
     */
    @FXML
    public void changeSceneForMenuItem(String fxml, String title) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(fxml));
        Parent parent = loader.load();
        Scene parentScene = new Scene(parent);

        Stage window = (Stage)tableView.getScene().getWindow();

        window.setScene(parentScene);
        window.setTitle(title);

        window.show();
    }


    /**
     * takes user to view for csv import
     */
    @FXML
    public void importFromCsvClicked(){
        try {
            changeSceneForMenuItem("csvImportPopUp.fxml", "Import from '.csv'-file");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Method for deletion of all patients with one click
     *
     * @throws IOException if not able to load fxml resource
     */
    @FXML
    public void deleteAllPatients() throws IOException {
        PatientRegisterInfo.setSelectedPatients(PatientRegisterInfo.getAllPatients());
        changeSceneForMenuItem("deletePatient.fxml", "WARNING you are about to delete ALL patients");
    }


    /**
     * Checks if there are any newly added patients (after original import)
     * If any, the user switches view to be prompted about export-selection (all or only the new ones)
     * Else, the user is switched directly over to the export view
     */
    @FXML
    public void exportToCsvClicked(){
        try {
            if(PatientRegisterInfo.getNewPatients().size() > 0){
                changeSceneForMenuItem("exportAllOrNew.fxml", "Choose files to export");
                //PopUpWindowController.setTotalPatientsLabel((String.valueOf(PatientRegisterInfo.getAllPatients().size())));
                //PopUpWindowController.setRecentPatientsLabel(String.valueOf(PatientRegisterInfo.getNewPatients().size()));
            }
            else {
                PatientRegisterInfo.setSelectedPatients(PatientRegisterInfo.getAllPatients());
                changeSceneForMenuItem("csvExportPopUp.fxml", "Export to '.csv'-file");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
