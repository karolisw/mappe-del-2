package ntnu.mappe2.karolisw;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * @Author Karoline Sund Wahl
 * @Version 1.0 05-05-2021
 */

public class App extends Application {
    private static Stage window;

    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("patientRegister.fxml"));

        //giving stage a more appropriate name
        window = primaryStage;
        window.setTitle("Patient Register");

        window.setResizable(false);


        window.setScene(new Scene(root,600,400));
        window.show();

    }


    public static void main(String[] args) {
        launch();
    }

}