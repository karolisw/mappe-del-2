package ntnu.mappe2.karolisw;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Class that shares functionality between being able to add new patient and edit existing patient
 * Therefore, button for adding new patient is hidden when editing patient and vice-versa
 */
public class PatientDetailsController implements Initializable {
    SceneChanger sceneChanger = new SceneChanger();

    // Labels
    @FXML Label firstNameLabel;
    @FXML Label lastNameLabel;
    @FXML Label socialSecurityNumberLabel;
    @FXML Label titleLabel;
    @FXML Label diagnosisLabel;
    @FXML Label generalPractitionerLabel;
    @FXML Label errorLabel;

    // TextFields
    @FXML TextField firstNameTextField;
    @FXML TextField lastNameTextField;
    @FXML TextField socialSecurityNumberTextField;
    @FXML TextField diagnosisTextField;
    @FXML TextField generalPractitionerTextField;

    // Buttons
    @FXML Button addPatientButton;
    @FXML Button editCommitButton;
    @FXML Button cancelButton;

    /**
     * On scene change onto this scene
     * The static field "addOrEditPatient" in SceneChanger class decides the layout of this scene
     * "addOrEditPatient" field is set in PatientRegisterController upon ActionEvent of clicking edit-button
     *
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        if(PatientRegisterInfo.getAddOrEditPatient().equals("edit")) {
            editPatient();
        }
        else {
            addPatient();
        }
    }

    /**
     * This method is called upon in initialize method
     * Fills TextFields with current information about patientToBeEdited (selected from register)
     * does not throw anything because of the pre-requirements to get into this scene
     */
    public void editPatient(){
        Patient patientToBeEdited = PatientRegisterInfo.getSelectedPatients().get(0);

        firstNameTextField.setText(patientToBeEdited.getFirstName());
        lastNameTextField.setText(patientToBeEdited.getLastName());
        socialSecurityNumberTextField.setText(patientToBeEdited.getSocialSecurityNumber());
        diagnosisTextField.setText(patientToBeEdited.getDiagnosis());
        generalPractitionerTextField.setText(patientToBeEdited.getGeneralPractitioner());

        addPatientButton.setVisible(false);
        titleLabel.setText("Patient details");
    }

    /**
     * User should not be able to edit patient without first adding it
     */
    public void addPatient(){
        editCommitButton.setVisible(false);
    }

    /**
     * Checks if fields are filled in (and filled in correctly)
     * adds new patient to list of all patients and to list of patients added during current runtime
     *
     * @param event on add button clicked
     */
    @FXML
    public void addPatientButtonClicked(ActionEvent event){
        if(!textFieldsFilledIn()){
            errorLabel.setText("First name, last name and number fields must be filled in");
        }
        else if(textFieldsFilledIn() && !socialSecurityNumberCheck()){
            errorLabel.setText("Social security number should be 11 digits long and only numbers");
        }
        else {
            Patient newPatient = new Patient(firstNameTextField.getText(), lastNameTextField.getText(),
                    diagnosisTextField.getText(), generalPractitionerTextField.getText(), socialSecurityNumberTextField.getText());
            PatientRegisterInfo.getAllPatients().add(newPatient);
            PatientRegisterInfo.getNewPatients().add(newPatient);
            try {
                sceneChanger.changeScene(event,"patientRegister.fxml", "Patient register");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Patient selected in register is deleted from list of all patients an added back after editing
     * @param event on confirm button clicked
     */
    @FXML
    public void editConfirmButtonClicked(ActionEvent event){
        try {
            PatientRegisterInfo.getAllPatients().remove(PatientRegisterInfo.getSelectedPatients().get(0));

            Patient patientToBeEdited = PatientRegisterInfo.getSelectedPatients().get(0);
            patientToBeEdited.setFirstName(firstNameTextField.getText());
            patientToBeEdited.setLastName(lastNameTextField.getText());
            patientToBeEdited.setSocialSecurityNumber(socialSecurityNumberTextField.getText());
            patientToBeEdited.setDiagnosis(diagnosisTextField.getText());
            patientToBeEdited.setGeneralPractitioner(generalPractitionerTextField.getText());

            PatientRegisterInfo.getAllPatients().add(patientToBeEdited);
            PatientRegisterInfo.removeSelectedPatients();
            PatientRegisterInfo.removeAddOrEditStatus();

            sceneChanger.changeScene(event,"patientRegister.fxml", "Patient register");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Transfers user back to register without performing any changes
     * @param event on cancel button clicked
     */
    @FXML
    public void cancelButtonClicked(ActionEvent event){
        try{
            PatientRegisterInfo.removeAddOrEditStatus();
            sceneChanger.changeScene(event,"patientRegister.fxml", "Patient register");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Supporting-method for addNewPatientButtonClicked-method
     * @return boolean true/false for if fields contain text
     */
    public boolean textFieldsFilledIn(){
        return !firstNameTextField.getText().isBlank() && !lastNameTextField.getText().isBlank() &&
                !socialSecurityNumberTextField.getText().isBlank();
    }

    /**
     * Checks for correct length and only-number composition in social security number
     * @return boolean true/false
     */
    public boolean socialSecurityNumberCheck(){
        if (!socialSecurityNumberTextField.getText().matches("[0-9]+")) {
            return false;
        }
        if(socialSecurityNumberTextField.getText().length() != 11){
            return false;
        }
        return true;
    }

}
