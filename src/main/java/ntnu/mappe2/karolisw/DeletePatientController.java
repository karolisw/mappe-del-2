package ntnu.mappe2.karolisw;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 * @Author Karoline Sund Wahl
 * @Version 1.0 05-05-2021
 * Class w/ functionality to delete one or more patients from register
 *
 */
public class DeletePatientController {
    SceneChanger sceneChanger = new SceneChanger();

    // Labels
    @FXML Label description;
    @FXML Label deleteConfirmation;

    // Buttons
    @FXML Button okButton;
    @FXML Button cancelButton;

    /**
     * On cancel button clicked, the user is transfered back to register
     *
     * @param event cancel - button clicked
     */
    @FXML
    public void cancelButtonClicked(ActionEvent event){
        try{
            PatientRegisterInfo.removeSelectedPatients();
            sceneChanger.changeScene(event, "patientRegister.fxml", "Patient register");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Method deletes list of selected patients from register
     *
     * @param event is the button click that initiates the method
     */
    @FXML
    public void ConfirmButtonClicked(ActionEvent event){
        try {
            PatientRegisterInfo.deleteSelectedPatients();
            PatientRegisterInfo.removeSelectedPatients();
            sceneChanger.changeScene(event, "patientRegister.fxml", "Patient register");

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
