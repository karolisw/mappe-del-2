package ntnu.mappe2.karolisw;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * A class with methods for easily changing scenes
 */
public class SceneChanger {

    /**
     * Method for changing a scene.
     * parameter holds the fxml file to switch over to
     * and the title to be set on the new scene.
     * Only works on events that touches elements of from javafx.scene.Node
     */
    public void changeScene(ActionEvent event, String fxml, String title) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(fxml));
        Parent parent = loader.load();
        Scene scene = new Scene(parent);

        Stage stage = (Stage) ((javafx.scene.Node) event.getSource()).getScene().getWindow();
        stage.setTitle(title);
        stage.setScene(scene);
        stage.show();
    }
}

