package ntnu.mappe2.karolisw;

import javafx.event.ActionEvent;
import java.io.IOException;

/**
 * Class only displays information about application
 */
public class HelpController {
    SceneChanger sceneChanger = new SceneChanger();

    /**
     * Method transfers user back to register with SceneChanger
     *
     * @param event ok button clicked
     */
    public void okButtonClicked(ActionEvent event){
        try {
            sceneChanger.changeScene(event,"patientRegister.fxml", "Patient register");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
