module ntnu.mappe2.karolisw {
    requires javafx.controls;
    requires javafx.fxml;

    opens ntnu.mappe2.karolisw to javafx.fxml;
    exports ntnu.mappe2.karolisw;
}